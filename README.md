# Readme

The app has somewhat been done to a point I think it represents a bit of my knowledge, but does need 2 more iterations.

TODO:

- implement loading and loaders for api mocked calls
- create a Item component that is used for Currency Select and Promo Select and also for the total indicator
- organize styles
- fix ui to reflect the design
- when clicking on change at some selected item, the item is not selected on select, only in the selected items. perhaps a rerender should be forced, if it's not a bug
- write tests for the flows
