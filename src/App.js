import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Checkout from "./features/checkout/Checkout";
import Step1 from "./features/checkout/steps/Step1";
import Step2 from "./features/checkout/steps/Step2";
import Step3 from "./features/checkout/steps/Step3";
import Step4 from "./features/checkout/steps/Step4";
import Success from "./features/checkout/steps/Success";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Checkout />}>
          <Route index element={<Step1 />} />
          <Route path="step1" element={<Step1 />} />
          <Route path="step2" element={<Step2 />} />
          <Route path="step3" element={<Step3 />} />
          <Route path="step4" element={<Step4 />} />
          <Route path="success" element={<Success />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
