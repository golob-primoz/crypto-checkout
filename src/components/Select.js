import React, { useState } from "react";
import styles from "../App.module.css";
import gray_circle from "../static/images/gray_circle.svg";
import blue_with_circle from "../static/images/blue_with_hook.svg";

export default function Select({ items, selectHandler, children }) {
  if (!children)
    throw Error(
      "must pass an element function as child for rendering children"
    );

  const [selectedItem, setSelectedItem] = useState({});
  return (
    <ul className={styles.select}>
      {items.map((
        item,
        index // can use index as key as its a readonly select
      ) => (
        <li
          className={styles["select-item"]}
          key={index}
          onClick={() => {
            selectHandler({ item });
            setSelectedItem(item);
          }}
        >
          <div>
            {selectedItem.id !== item.id ? (
              <img src={gray_circle} />
            ) : (
              <img src={blue_with_circle} />
            )}
            <span className={styles["select-text"]}>{children(item)}</span>
          </div>
        </li>
      ))}
    </ul>
  );
}
