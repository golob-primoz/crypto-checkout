import React, { useState } from "react";
import styles from "../App.module.css";

export default function InputWithCopy({ value }) {
  const [buttonText, setButonText] = useState("Copy");
  return (
    <div className={styles["input-with-copy"]}>
      <div className={styles["input-wrapper"]}>
        <input type={"text"} value={value} readOnly />
      </div>
      <div className={styles.action}>
        <button
          className={styles["blue-button"]}
          onClick={() => {
            navigator.clipboard
              .writeText(value)
              .then(() => setButonText("Copied!"));
          }}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
}
