import React from "react";
import styles from "../../../App.module.css";

export default function NextButton({ handler, children }) {
  return (
    <div className={styles.actions}>
      <button className={styles["blue-button"]} onClick={handler}>
        {children}
      </button>
    </div>
  );
}
