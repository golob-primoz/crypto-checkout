import React from "react";
import Item from "./Item";
import styles from "../Checkout.module.css";

export default function TotalSection({ selectedItem, price }) {
  return (
    <div className="total">
      <label className={styles["select-crypto-text"]}>Total</label>
      {selectedItem && (
        <Item
          currency={selectedItem.name}
          amount={selectedItem.conversion_rate * price}
        />
      )}
    </div>
  );
}
