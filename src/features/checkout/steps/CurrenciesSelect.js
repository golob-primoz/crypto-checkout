import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Select from "../../../components/Select";
import { selectCurrency, setCurrencies } from "../checkoutSlice";
import styles from "../Checkout.module.css";

export default function CurrenciesSelect() {
  const dispatch = useDispatch();
  const currencies = useSelector((state) => state.checkout.currencies);
  const getCurrencies = () => {
    // fetch currencies
    return new Promise((res) =>
      res([
        { id: 1, name: "eth", conversion_rate: 0.00003442 },
        { id: 2, name: "btc", conversion_rate: 0.00046642 },
        { id: 3, name: "usdt", conversion_rate: 1.05622506 },
        { id: 4, name: "bnb", conversion_rate: 0.003468 },
      ])
    );
  };

  useEffect(() => {
    getCurrencies().then((inCurrencies) => {
      console.log("inCurrencies", inCurrencies);
      dispatch(setCurrencies(inCurrencies));
    });
  }, []);

  return (
    <div className="select-criptocurrency">
      <label className={styles["select-crypto-text"]}>
        Select your cryptocurrency
      </label>
      <Select
        items={currencies}
        selectHandler={({ item }) => {
          // dispatch set selected item
          console.log("selected item", item);
          dispatch(selectCurrency(item));
        }}
      >
        {(item) => item.name}
      </Select>
    </div>
  );
}
