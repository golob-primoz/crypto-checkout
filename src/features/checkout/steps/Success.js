import React from "react";
import appStyles from "../../../App.module.css";
import cStyles from "../Checkout.module.css";
import StepWrapper from "./StepWrapper";
import { useNavigate } from "react-router";
import check from "../../../static/images/check.svg";

export default function Success() {
  const navigate = useNavigate();
  return (
    <StepWrapper>
      <div className={cStyles["success-frame"]}>
        <div className={cStyles["container"]}>
          <div className={cStyles["row"]}>
            <div className={cStyles["frame"]}>
              <div className={cStyles["item"]}>
                <header>
                  <div className="check-icon">
                    <img src={check} />
                  </div>
                  <h1 className={cStyles["select-crypto-text-h1"]}>
                    Payment successful
                  </h1>
                </header>
              </div>
              <div className={cStyles["item"]}>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris.
                </p>
              </div>
            </div>
          </div>
          <div className={cStyles["row"]}>
            <div className={cStyles["item"]}>
              <button
                className={appStyles["blue-button"]}
                onClick={() => {
                  navigate("/");
                }}
              >
                Home
              </button>
            </div>
          </div>
        </div>
      </div>
    </StepWrapper>
  );
}
