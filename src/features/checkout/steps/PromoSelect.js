import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Select from "../../../components/Select";
import { selectPromo, setPromo } from "../checkoutSlice";
import styles from "../Checkout.module.css";

export default function PromoSelect() {
  const dispatch = useDispatch();
  const promo = useSelector((state) => state.checkout.promo);
  const getPromo = () => {
    // fetch currencies
    return new Promise((res) =>
      res([
        { id: 1, name: "FREE SHIPPING" },
        { id: 2, name: "10% OFF NEXT ORDER" },
      ])
    );
  };

  useEffect(() => {
    getPromo().then((inPromo) => {
      console.log("inPromo", inPromo);
      dispatch(setPromo(inPromo));
    });
  }, []);

  return (
    <div className="promo">
      <h2 className={styles["select-crypto-text"]}>Select promo</h2>
      <Select
        items={promo}
        selectHandler={({ item }) => {
          // dispatch set selected item
          console.log("selected item", item);
          dispatch(selectPromo(item));
        }}
      >
        {(item) => item.name}
      </Select>
    </div>
  );
}
