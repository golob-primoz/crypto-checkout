import React from "react";
import { useNavigate } from "react-router";
import styles from "../../../App.module.css";
import blue_with_circle from "../../../static/images/blue_with_hook.svg";

export default function SelectedItemsList({ items }) {
  console.log("selectedItemsList", items);
  const navigate = useNavigate();
  if (items && items.length === 0) return "";

  return (
    <div className="selected-items">
      <ul className={styles.select}>
        {items.map((item, index) => {
          return (
            <li className={styles[item.type]} key={index}>
              <div className={styles.left}>
                <img src={blue_with_circle} />
                <span className={styles["select-text"]}>{item.name}</span>
              </div>

              <div className={styles.right}>
                <button
                  className={styles["change-button"]}
                  onClick={() => {
                    navigate(item.redirectUrl);
                  }}
                >
                  change
                </button>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
