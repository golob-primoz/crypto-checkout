import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import InputWithCopy from "../../../components/InputWithCopy";
import { sendPaymentData } from "../checkoutAPI";
import { setPaymentAddress } from "../checkoutSlice";
import NextButton from "./NextButton";
import SelectedItemsList from "./SelectedItemsList";
import StepWrapper from "./StepWrapper";
import TotalSection from "./TotalSection";
import styles from "../Checkout.module.css";

export default function Step3() {
  const navigate = useNavigate();
  const selectedItems = useSelector((state) => state.checkout.selectedItems);
  const price = useSelector((state) => state.checkout.price);
  const dispatch = useDispatch();
  const paymentAddress = useSelector((state) => state.checkout.paymentAddress);

  useEffect(() => {
    if (!selectedItems[0]) {
      console.log("you need to set currency, redirecting you to first step");
      navigate("/");
    }
    if (!selectedItems[1]) {
      console.log("you need to set currency, redirecting you to second step");
      navigate("/step2");
    }
  }, [selectedItems]);

  return (
    <StepWrapper>
      {!!selectedItems.length && <SelectedItemsList items={selectedItems} />}
      <TotalSection selectedItem={selectedItems[0]} price={price} />
      <div className="payment-address">
        <h2 className={styles["select-crypto-text"]}>Send your payment to</h2>
        <InputWithCopy value={paymentAddress} />
      </div>

      <NextButton
        handler={() => {
          // send data to api
          sendPaymentData({
            currencyId: selectedItems[0].id,
            amount: selectedItems[0].price * selectedItems[0].conversion_rate,
            promoId: selectedItems[1].id,
          }).then((response) => {
            if (
              response.status === 200 &&
              response.data &&
              response.data.payment_address
            ) {
              dispatch(setPaymentAddress(response.data.payment_address));
              navigate("/Step4");
            }
          });
        }}
      >
        Next
      </NextButton>
    </StepWrapper>
  );
}
