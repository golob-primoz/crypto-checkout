import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { sendPaymentData } from "../checkoutAPI";
import { setPaymentAddress } from "../checkoutSlice";
import NextButton from "./NextButton";
import PromoSelect from "./PromoSelect";
import SelectedItemsList from "./SelectedItemsList";
import StepWrapper from "./StepWrapper";
import TotalSection from "./TotalSection";

export default function Step2() {
  const navigate = useNavigate();
  const selectedItems = useSelector((state) => state.checkout.selectedItems);
  const price = useSelector((state) => state.checkout.price);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!selectedItems[0]) {
      console.log("you need to set currency, redirecting you to first step");
      navigate("/");
    }
  }, [selectedItems]);

  return (
    <StepWrapper>
      {!!selectedItems.length && <SelectedItemsList items={selectedItems} />}
      <TotalSection selectedItem={selectedItems[0]} price={price} />
      <PromoSelect />

      <NextButton
        handler={() => {
          // send data to api
          if (selectedItems[0] == null || selectedItems[1] == null) {
            return; // write out validation message
          }
          sendPaymentData({
            currencyId: selectedItems[0].id,
            amount: selectedItems[0].price * selectedItems[0].conversion_rate,
            promoId: selectedItems[1].id,
          }).then((response) => {
            if (
              response.status === 200 &&
              response.data &&
              response.data.payment_address
            ) {
              dispatch(setPaymentAddress(response.data.payment_address));
              navigate("/Step3");
            }
          });
        }}
      >
        Next
      </NextButton>
    </StepWrapper>
  );
}
