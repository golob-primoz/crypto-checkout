import React from "react";
import styles from "../../../App.module.css";

export default function Item({ currency, amount }) {
  return (
    <div className={styles["select-item"]}>
      {currency} {amount}
    </div>
  );
}
