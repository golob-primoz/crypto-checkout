import React from "react";

export default function StepWrapper({ children }) {
  return <div className="step-wrapper">{children}</div>;
}
