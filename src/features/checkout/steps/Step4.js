import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { getPaymentConfirmation } from "../checkoutAPI";
import appStyles from "../../../App.module.css";
import cStyles from "../Checkout.module.css";
import StepWrapper from "./StepWrapper";
import refresh from "../../../static/images/refresh.svg";

export default function Step4() {
  const navigate = useNavigate();
  const paymentAddress = useSelector((state) => state.checkout.paymentAddress);
  return (
    <StepWrapper>
      <div className={cStyles["pending-frame"]}>
        <div className={cStyles["container"]}>
          <div className={cStyles["row"]}>
            <div className={cStyles["frame"]}>
              <div className={cStyles["item"]}>
                <header>
                  <div className="pending-icon">
                    <img src={refresh} />
                  </div>
                  <h1 className={cStyles["select-crypto-text-h1"]}>
                    Transaction pending
                  </h1>
                </header>
              </div>
              <div className={cStyles["item"]}>
                <p>Transaction ID</p>
              </div>
              <div className={cStyles["item"]}>
                <p>{paymentAddress}</p>
              </div>
              <div className={cStyles["item"]}>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod...
                </p>
              </div>
            </div>
          </div>
          <div className={cStyles["row"]}>
            <div className={cStyles["item"]}>
              <button
                className={appStyles["blue-button"]}
                onClick={() => {
                  getPaymentConfirmation().then(() => navigate("/success"));
                }}
              >
                refresh
              </button>
            </div>
          </div>
        </div>
      </div>
    </StepWrapper>
  );
}
