import React from "react";
import { useSelector } from "react-redux";
import CurrenciesSelect from "./CurrenciesSelect";
import { useNavigate } from "react-router-dom";
import SelectedItemsList from "./SelectedItemsList";
import TotalSection from "./TotalSection";
import StepWrapper from "./StepWrapper";
import NextButton from "./NextButton";

export default function Step1() {
  const navigate = useNavigate();
  const selectedItems = useSelector((state) => state.checkout.selectedItems);
  const price = useSelector((state) => state.checkout.price);
  console.log(selectedItems, "items");

  return (
    <StepWrapper>
      {!!selectedItems.length && <SelectedItemsList items={selectedItems} />}
      <CurrenciesSelect />

      <TotalSection selectedItem={selectedItems[0]} price={price} />
      <NextButton
        handler={() => {
          if (selectedItems[0] == null) {
            return; // write out validation message
          }
          navigate("/step2");
        }}
      >
        Next
      </NextButton>
    </StepWrapper>
  );
}
