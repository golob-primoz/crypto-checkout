const sendPaymentData = ({ currencyId, amount, promoId }) => {
  return new Promise((res) => {
    res({
      status: 200,
      data: {
        payment_address: "n33YzZo9UNwxbJe5r5qSHePdfXqZLNPChx",
      },
    });
  });
};

const getPaymentConfirmation = () => {
  return new Promise((res) => res({ status: 201 }));
};

export { sendPaymentData, getPaymentConfirmation };
