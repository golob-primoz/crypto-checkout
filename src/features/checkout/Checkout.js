import React from "react";
import { Outlet } from "react-router-dom";
import styles from "./Checkout.module.css";
import bitpay from "../../static/images/bitpay.svg";

export default function Checkout() {
  return (
    <section className={styles.content}>
      <div className={styles.box}>
        <div className={styles["main-wrapper"]}>
          <div className={styles["main-wrapper-content"]}>
            <h1>Pay with crypto</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <Outlet />
          </div>
        </div>
        <div className={styles["side-wrapper"]}>
          <div className={styles["side-wrapper-content"]}>
            <div className={styles["content-column"]}>
              <div className={styles["select-crypto-text"]}>
                Easy, fast and secure payments
              </div>
              <div className={styles["text"]}>
                Excepteur sint occaecat cupidatat non proident, sunt in culpa
                qui officia deserunt mollit anim id est laborum.
              </div>
              <div className={styles["powered-by"]}>
                <p>Powered by</p>
                <img src={bitpay} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
