import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  currencies: [],
  promo: [],
  selectedCurrency: null,
  selectedPromo: null,
  selectedItems: [],
  price: 100, // ammount to pay in euro
  step: 0,
  paymentAddress: null,
};

export const checkoutSlice = createSlice({
  name: "checkout",
  initialState,
  reducers: {
    selectCurrency: (state, action) => {
      state.selectedItems[0] = {
        ...action.payload,
        redirectUrl: "/step1",
        type: "select-item",
      };
    },
    setCurrencies: (state, action) => {
      state.currencies = action.payload;
    },
    selectPromo: (state, action) => {
      state.selectedItems[1] = {
        ...action.payload,
        redirectUrl: "/step2",
        type: "select-item",
      };
    },
    setPromo: (state, action) => {
      state.promo = action.payload;
    },
    setStep: (state, action) => {
      state.step = action.payload;
    },
    setPaymentAddress: (state, action) => {
      state.paymentAddress = action.payload;
    },
  },
});

export const {
  selectCurrency,
  setCurrencies,
  selectPromo,
  setPromo,
  setStep,
  setPaymentAddress,
} = checkoutSlice.actions;
export default checkoutSlice.reducer;
